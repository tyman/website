# My website

This is just a basic personal website I made using the [astro](https://astro.build/) framework. It generates a purely static webpage, no SSR or CSR.

## Tools/libraries used

- [astro](https://astro.build/) for generating the HTML
- [scss](https://sass-lang.com/) for making CSS slightly nicer to use
- [prettier](https://prettier.io) for auto-formatting files
- [typescript](https://typescriptlang.org/) because javascript sucks

## Contributing

No clue why anyone besides me would want to contribute, but simply make a PR or issue and i'll look at it I suppose. This node project uses [corepack](https://nodejs.org/api/corepack.html) so just run `corepack enable` and then use `yarn` to manage packages and run scripts. Running `yarn` by itself will also install all packages needed.

Scripts:

- `yarn dev`: Starts a hot-reloading development webserver with astro
- `yarn preview`: Starts a test webserver serving the `dist` directory with astro
- `yarn build`: Builds the static HTML and other files into the `dist` directory with astro
- `yarn format`: Formats all files with prettier
